package pl.nn.demoapp.useraccount;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.math.BigDecimal;

@Service
class UserAccountFactory {

    UserAccount create(String name, String surname, BigDecimal amount) {
        Assert.notNull(amount, "amount can not be null");
        Assert.notNull(name, "name can not be null");
        Assert.notNull(surname, "surname can not be null");
        Assert.isTrue(amount.compareTo(BigDecimal.ZERO) > 0, "amount must be grater then zero");

        return UserAccount.builder()
                .personalData(PersonalData.builder()
                        .name(name)
                        .surname(surname)
                        .build())
                .currencyAccounts(CurrencyAccounts.from(amount))
                .build();
    }
}
