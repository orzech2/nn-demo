package pl.nn.demoapp.useraccount;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder(toBuilder = true)
class UserAccount {
    private final String identity;
    private final PersonalData personalData;
    private final CurrencyAccounts currencyAccounts;
}
