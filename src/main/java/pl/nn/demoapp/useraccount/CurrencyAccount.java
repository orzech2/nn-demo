package pl.nn.demoapp.useraccount;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;

@Getter
@RequiredArgsConstructor
public class CurrencyAccount {
    private final CurrencyType currencyType;
    private final BigDecimal amount;

    CurrencyAccount withdraw(BigDecimal amount) {
        BigDecimal afterWithdraw = this.amount.subtract(amount);
        if (afterWithdraw.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("not enough money");
        }
        return new CurrencyAccount(this.currencyType, afterWithdraw);
    }

    CurrencyAccount deposit(BigDecimal amount) {
        return new CurrencyAccount(this.currencyType, this.amount.add(amount));
    }
}
