package pl.nn.demoapp.useraccount;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class PersonalData {
    private final String name;
    private final String surname;
}
