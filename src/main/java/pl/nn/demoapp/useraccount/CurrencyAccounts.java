package pl.nn.demoapp.useraccount;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import pl.nn.demoapp.exchangerate.Currency;
import pl.nn.demoapp.exchangerate.Rates;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
class CurrencyAccounts {
    private final Map<CurrencyType, CurrencyAccount> allCurrencyAccounts;

    CurrencyAccount getCurrencyAccount(final CurrencyType currencyType) {
        return allCurrencyAccounts.get(currencyType);
    }

    synchronized boolean exchange(final BigDecimal amount, final Rates rates) {
        final CurrencyAccount sourceAfterWithdraw = getCurrencyAccount(rates.getSell().getCurrency())
                .withdraw(amount);
        final CurrencyAccount destinyAfterDeposit = getCurrencyAccount(rates.getBuy().getCurrency())
                .deposit(amount
                        .multiply(rates.getSell().getRate())
                        .divide(rates.getBuy().getRate(), 2, RoundingMode.HALF_DOWN));
        allCurrencyAccounts.putAll(collectAsMap(sourceAfterWithdraw, destinyAfterDeposit));
        return true;
    }

    private CurrencyAccount getCurrencyAccount(final Currency currency) {
        return getCurrencyAccount(CurrencyType.valueOf(currency.name()));
    }

    static CurrencyAccounts from(final BigDecimal amount) {
        return new CurrencyAccounts(collectAsMap(
                new CurrencyAccount(CurrencyType.PLN, amount),
                new CurrencyAccount(CurrencyType.USD, BigDecimal.ZERO)
        ));
    }

    private static Map<CurrencyType, CurrencyAccount> collectAsMap(final CurrencyAccount... accounts) {
        return Stream
                .of(accounts)
                .collect(Collectors.toMap(CurrencyAccount::getCurrencyType, Function.identity()));
    }
}
