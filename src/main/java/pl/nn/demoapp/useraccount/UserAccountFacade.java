package pl.nn.demoapp.useraccount;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.nn.demoapp.exchangerate.Currency;
import pl.nn.demoapp.exchangerate.ExchangeRateFacade;
import pl.nn.demoapp.interfaces.provider.rest.dto.CurrencyTypeDTO;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class UserAccountFacade {

    private final UserAccountRepository userAccountRepository;
    private final UserAccountFactory userAccountFactory;
    private final ExchangeRateFacade exchangeRateFacade;

    public String createAccount(String name, String surname, BigDecimal amount) {
        UserAccount userAccount = userAccountFactory.create(name, surname, amount);
        return userAccountRepository
                .save(userAccount)
                .getIdentity();
    }

    public PersonalData getPersonalData(String accountIdentity) {
        return userAccountRepository
                .findById(accountIdentity)
                .map(UserAccount::getPersonalData)
                .orElseThrow(() -> new IllegalArgumentException("account does not exists"));
    }

    public CurrencyAccount getCurrencyAccount(String accountIdentity, CurrencyType currencyType) {
        return userAccountRepository
                .findById(accountIdentity)
                .map(UserAccount::getCurrencyAccounts)
                .map(currencyBalances -> currencyBalances.getCurrencyAccount(currencyType))
                .orElseThrow(() -> new IllegalArgumentException("currency account does not exists"));
    }

    public void exchangeOrder(String accountIdentity, CurrencyTypeDTO sourceCurrency, CurrencyTypeDTO destinationCurrency, BigDecimal amount) {
        userAccountRepository
                .findById(accountIdentity)
                .map(UserAccount::getCurrencyAccounts)
                .map(currencyAccounts -> currencyAccounts.exchange(
                        amount,
                        exchangeRateFacade.getRatesFor(Currency.valueOf(sourceCurrency.name()), Currency.valueOf(destinationCurrency.name())))
                )
                .orElseThrow(() -> new IllegalArgumentException("exchange cannot be done"));
    }
}
