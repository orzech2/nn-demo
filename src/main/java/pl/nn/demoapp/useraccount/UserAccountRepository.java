package pl.nn.demoapp.useraccount;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Service
class UserAccountRepository {
    private final Map<String, UserAccount> userAccounts = new ConcurrentHashMap<>();

    UserAccount save(final UserAccount userAccount) {
        Assert.isNull(userAccount.getIdentity(), "user account is already saved");
        final String identity = UUID.randomUUID().toString();
        return userAccounts.merge(
                identity,
                userAccount.toBuilder()
                        .identity(identity)
                        .build(),
                (_1, _2) -> {
                    throw new IllegalArgumentException("Account already exists");
                }
        );
    }

    public Optional<UserAccount> findById(final String accountIdentity) {
        return Optional.ofNullable(userAccounts.get(accountIdentity));
    }
}
