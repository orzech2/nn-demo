package pl.nn.demoapp.interfaces.provider.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.nn.demoapp.interfaces.provider.rest.api.UserAccountApi;
import pl.nn.demoapp.interfaces.provider.rest.dto.*;
import pl.nn.demoapp.useraccount.CurrencyAccount;
import pl.nn.demoapp.useraccount.CurrencyType;
import pl.nn.demoapp.useraccount.PersonalData;
import pl.nn.demoapp.useraccount.UserAccountFacade;

import java.net.URI;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class UserAccountResource implements UserAccountApi {

    private final UserAccountFacade userAccountFacade;

    @Override
    public ResponseEntity<UserAccountDTO> getUserAccount(String accountIdentity) {
        PersonalData personalData = userAccountFacade.getPersonalData(accountIdentity);
        return ResponseEntity.ok(UserAccountDTO.builder()
                .name(personalData.getName())
                .surname(personalData.getSurname())
                .build());
    }

    @Override
    public ResponseEntity<AccountIdentityDTO> createUserAccount(CreateUserAccountDTO createUserAccountDTO) {
        String accountIdentity = userAccountFacade.createAccount(createUserAccountDTO.getName(), createUserAccountDTO.getSurname(), createUserAccountDTO.getAmount());
        URI accountUri = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path(accountIdentity)
                .build()
                .toUri();
        return ResponseEntity
                .created(accountUri)
                .body(new AccountIdentityDTO(accountIdentity));
    }

    @Override
    public ResponseEntity<CurrencyAccountDTO> getUserAccountBalance(String accountIdentity, CurrencyTypeDTO currencyType) {
        CurrencyAccount currencyAccount = userAccountFacade.getCurrencyAccount(accountIdentity, CurrencyType.valueOf(currencyType.name()));
        return ResponseEntity.ok(CurrencyAccountDTO.builder()
                .amount(currencyAccount.getAmount())
                .currencyType(CurrencyTypeDTO.valueOf(currencyAccount.getCurrencyType().name()))
                .build());
    }

    @Override
    public ResponseEntity<Void> exchangeOrder(String accountIdentity, CurrencyTypeDTO currencyType, ExchangeOrderDTO exchangeOrderDTO) {
        userAccountFacade.exchangeOrder(accountIdentity, currencyType, exchangeOrderDTO.getDetinationCurrencyType(), exchangeOrderDTO.getAmount());
        return ResponseEntity.noContent().build();
    }
}
