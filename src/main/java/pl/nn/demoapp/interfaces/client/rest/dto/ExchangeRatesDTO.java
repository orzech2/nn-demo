package pl.nn.demoapp.interfaces.client.rest.dto;

import lombok.Data;

import java.util.List;

@Data
public class ExchangeRatesDTO {
    private List<Rate> rates;
}
