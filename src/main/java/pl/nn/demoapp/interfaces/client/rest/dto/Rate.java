package pl.nn.demoapp.interfaces.client.rest.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Rate {
    private BigDecimal bid;
    private BigDecimal ask;
}
