package pl.nn.demoapp.interfaces.client.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.nn.demoapp.config.NbpConfiguration;
import pl.nn.demoapp.interfaces.client.rest.dto.ExchangeRatesDTO;

import java.math.BigDecimal;

@RequiredArgsConstructor
@Service
public class NbpRestClient {

    private final RestTemplate nbpRestTemplate;
    private final NbpConfiguration nbpConfiguration;

    public BigDecimal getSellRate(String currency) {
        return nbpRestTemplate
                .getForObject(nbpConfiguration.getExchangeRates(), ExchangeRatesDTO.class, currency)
                .getRates()
                .iterator()
                .next()
                .getBid();
    }

    public BigDecimal getBuyRate(String currency) {
        return nbpRestTemplate
                .getForObject(nbpConfiguration.getExchangeRates(), ExchangeRatesDTO.class, currency)
                .getRates()
                .iterator()
                .next()
                .getAsk();
    }
}
