package pl.nn.demoapp.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@NoArgsConstructor
@ConfigurationProperties(prefix = "nbp", ignoreUnknownFields = false)
public class NbpConfiguration {

    private String exchangeRates;
}
