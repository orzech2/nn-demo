package pl.nn.demoapp.exchangerate;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@Builder
public class CurrencyRate {
    private final Currency currency;
    private final BigDecimal rate;
}
