package pl.nn.demoapp.exchangerate;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Rates {
    private final CurrencyRate sell;
    private final CurrencyRate buy;
}
