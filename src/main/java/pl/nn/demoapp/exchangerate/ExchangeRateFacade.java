package pl.nn.demoapp.exchangerate;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.nn.demoapp.interfaces.client.rest.NbpRestClient;

import java.math.BigDecimal;
import java.util.function.Function;

@RequiredArgsConstructor
@Service
public class ExchangeRateFacade {

    private final NbpRestClient nbpRestClient;

    public Rates getRatesFor(final Currency sell, final Currency buy) {
        return Rates.builder()
                .sell(getCurrencyRate(nbpRestClient::getSellRate, sell))
                .buy(getCurrencyRate(nbpRestClient::getBuyRate, buy))
                .build();
    }

    private CurrencyRate getCurrencyRate(final Function<String, BigDecimal> rateType, final Currency currency) {
        return new CurrencyRate(
                currency,
                currency == Currency.PLN
                        ? BigDecimal.ONE
                        : rateType.apply(currency.name())
        );
    }
}
